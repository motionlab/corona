FROM python

ENV DEBIAN_FRONTEND="noninteractive" 
RUN  useradd -r -u 1001 appuser

COPY requirements.txt requirements.txt
RUN  pip  install -r  requirements.txt

RUN mkdir /app
RUN mkdir /home/appuser
RUN chown appuser /app
RUN chown appuser /home/appuser

WORKDIR /app
USER    appuser

ADD Corona.py      /app/Corona.py
ADD templates      /app/templates
ADD population.csv /app/population.csv
CMD python         Corona.py
