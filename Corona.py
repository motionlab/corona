from   math              import sqrt
from   datetime          import datetime
import numpy             as     np
import pandas            as     pd
import matplotlib.pyplot as plt
import requests
import jinja2
from   os                     import walk
from   os                     import path
from   calendar               import monthrange
from   dateutil.relativedelta import relativedelta

def create_ts(df):
  ts         = df
  ts         = ts.drop(['Province/State', 'Country/Region','Lat', 'Long',' Population '], axis = 1)
  ts.set_index('region')
  ts         = ts.T
  ts.columns = ts.loc['region']
  ts         = ts.drop('region')
  ts         = ts.fillna(0)
  ts         = ts.reindex(sorted(ts.columns), axis=1)
  return (ts)

url       = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'
confirmed = pd.read_csv(url, error_bad_lines=False)
url       = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv'
death     = pd.read_csv(url, error_bad_lines=False)
url       = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv'
recover   = pd.read_csv(url, error_bad_lines=False)

confirmed['Country/Region'] = confirmed['Country/Region'].str.replace("Mainland China", "China")
confirmed['Country/Region'] = confirmed['Country/Region'].str.replace("US", "Unites States")
death['Country/Region']     = death['Country/Region'].str.replace("Mainland China", "China")
death['Country/Region']     = death['Country/Region'].str.replace("US", "Unites States")
recover['Country/Region']   = recover['Country/Region'].str.replace("Mainland China", "China")
recover['Country/Region']   = recover['Country/Region'].str.replace("US", "Unites States")
population                  = pd.read_csv('population.csv', sep=',', encoding='latin1')
confirmed                   = pd.merge(confirmed, population, how='left', on=['Province/State', 'Country/Region'])
death                       = pd.merge(death,     population, how='left', on=['Province/State', 'Country/Region'])
recover                     = pd.merge(recover,   population, how='left', on=['Province/State', 'Country/Region'])
confirmed['region']         = confirmed['Country/Region'].map(str)+ '_'+confirmed['Province/State'].map(str)
confirmed['region']         = confirmed['region'].str.replace("_nan", "")
death['region']             = death['Country/Region'].map(str)    + '_'+death['Province/State'].map(str)
death['region']             = death['region'].str.replace("_nan", "")
recover['region']           = recover['Country/Region'].map(str)  + '_'+recover['Province/State'].map(str)
recover['region']           = recover['region'].str.replace("_nan", "")
ts                          = create_ts(confirmed)
ts_d                        = create_ts(death)
ts_rec                      = create_ts(recover)
doubled                     = ts.copy()
delta                       = ts.copy()
delta_d                     = ts.copy()
new_vs_existing             = pd.DataFrame()
new_vs_existing_d           = pd.DataFrame()
average_of_4_days           = ts.copy()
average_of_7_days           = ts.copy()
reff4                       = ts.copy()
reff7                       = ts.copy()
days_to_sum                 = 7
now_string                  = datetime.now().strftime("%d-%m-%Y")

for date in range(ts.shape[0]):
  for country in range(0, ts.shape[1]):
    if ts.iat[date, country] > 0:
      for j in range(date, 0, -1):
        if ts.iat[j, country] <= (ts.iat[date, country] / 2):
          doubled.iat[date, country] = date - j
          break

for country in range(0, ts.shape[1]):
  for date in range(0, ts.shape[0]):
    delta.iat[date, country] = ts.iat[date, country] - ts.iat[date -1 , country]
    delta_d.iat[date, country] = ts_d.iat[date, country] - ts_d.iat[date -1 , country]
  delta.iat[0, country] = 0
  delta_d.iat[0, country] = 0

for country in range(ts.shape[1]):
  for date in range(days_to_sum, ts.shape[0]):
     val = ts.iat[date, country] - ts.iat[date - days_to_sum , country]
     new_vs_existing.at[ts.iat[date, country], ts.columns[country]] = max(0, val)
     val = ts_d.iat[date, country] - ts_d.iat[date - days_to_sum , country]
     new_vs_existing_d.at[ts_d.iat[date, country], ts_d.columns[country]] = max(0, val)

for country in range(ts.shape[1]):
  for date in range(4, ts.shape[0]):
     v = 0
     for i in range(date - 4, date):
       v += delta.iat[i, country]
     average_of_4_days.iat[date, country] = v / 4

for country in range(ts.shape[1]):
  reff4 = reff4.astype(float)
  for date in range(4, ts.shape[0]):
    if average_of_4_days.iat[date - 4, country] == 0:
        reff4.iat[date, country] = 0
    else:
        reff4.iat[date, country] = max(0, min(2, average_of_4_days.iat[date, country] / average_of_4_days.iat[date - 4, country]))

for country in range(ts.shape[1]):
  for date in range(7, ts.shape[0]):
     v = 0
     for i in range(date - 7, date):
       v += delta.iat[i, country]
     average_of_7_days.iat[date, country] = v / 7

for country in range(ts.shape[1]):
  reff7 = reff7.astype(float)
  for date in range(7, ts.shape[0]):
    if average_of_7_days.iat[date - 7, country] == 0:
        reff7.iat[date, country] = 0
    else:
        reff7.iat[date, country] = max(0, min(2, average_of_7_days.iat[date, country] / average_of_7_days.iat[date - 7, country]))

n = ts.reindex(ts.max().sort_values(ascending=False).index, axis = 1).columns.values
germanid = np.where(n == 'Germany')[0][0]

n = ts.reindex(ts.max().sort_values(ascending=False).index, axis = 1).columns.values
reff4[n].iloc[-50:, germanid:germanid+1].plot(marker='*', figsize=(20,6), secondary_y=True, legend=False).set_title('Magic number R (4 days, [0:2] capped) as of %s' % now_string, fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/R4.png", bbox_inches='tight')

n = ts.reindex(ts.max().sort_values(ascending=False).index, axis = 1).columns.values
reff7[n].iloc[-150:, germanid:germanid+1].plot(marker='*', figsize=(20,6), secondary_y=True, legend=False).set_title('Magic number R (7 days, [0:2] capped) as of %s' % now_string, fontdict={'fontsize': 22})
plt.savefig("/host/R7.png", bbox_inches='tight')

n = ts.reindex(ts.max().sort_values(ascending=False).index, axis = 1).columns.values
doubled[n].iloc[50:, germanid:germanid+1].plot(marker='*', figsize=(20,6), secondary_y=True, legend=False).set_title('Days until confirmed doubled as of %s' % now_string, fontdict={'fontsize': 22})
plt.savefig("/host/Doubled.png", bbox_inches='tight')

n = ts.reindex(ts.iloc[[0, -1]].max().sort_values(ascending=False).index, axis = 1).columns.values
new_vs_existing[n].iloc[0:,0: 3].plot(marker='*', figsize=(20,6), secondary_y=True, legend=False).set_title('New (sum of last %d days) vs existing Infected 1-3 as of %s' % (days_to_sum, now_string), fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/New-vs-Existing-0-2.png", bbox_inches='tight')

n = ts.reindex(ts.iloc[[0, -1]].max().sort_values(ascending=False).index, axis = 1).columns.values
new_vs_existing[n].iloc[0:,3:10].plot(marker='*', figsize=(20,6), legend=False, ylim=(0, 1500000)).set_title('New (sum of last %d days) vs existing Infected 4-10 as of %s' % (days_to_sum, now_string), fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/New-vs-Existing-3-10.png", bbox_inches='tight')

new_vs_existing[n].iloc[0:,10:20].plot(marker='*', figsize=(20,6), secondary_y=True, legend=False).set_title('New (sum of last %d days) vs existing Infected 11-20 as of %s' % (days_to_sum, now_string), fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/New-vs-Existing-10-20.png", bbox_inches='tight')

new_vs_existing[n].iloc[0:,20:30].plot(marker='*', figsize=(20,6), secondary_y=True, legend=False).set_title('New (sum of last %d days) vs existing Infected 21-30 as of %s' % (days_to_sum, now_string), fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/New-vs-Existing-20-30.png", bbox_inches='tight')

n = ts.reindex(ts_d.iloc[[0, -1]].max().sort_values(ascending=False).index, axis = 1).columns.values
new_vs_existing_d[n].iloc[0:,0:5].plot(marker='*', figsize=(20,6), secondary_y=True, legend=False).set_title('New (sum of last %d days) vs existing Death 1-5 as of %s' % (days_to_sum, now_string), fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/New-Death-vs-Existing-0-3.png", bbox_inches='tight')

n = ts.reindex(ts_d.iloc[[0, -1]].max().sort_values(ascending=False).index, axis = 1).columns.values
new_vs_existing_d[n].iloc[0:,5:10].plot(marker='*', figsize=(20,6), secondary_y=True, legend=False).set_title('New (sum of last %d days) vs existing Death 6-10 as of %s' % (days_to_sum, now_string), fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/New-Death-vs-Existing-4-10.png", bbox_inches='tight')

new_vs_existing_d[n].iloc[0:,10:20].plot(marker='*', figsize=(20,6), secondary_y=True, legend=False).set_title('New (sum of last %d days) vs existing Death 11-20 as of %s' % (days_to_sum, now_string), fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/New-Death-vs-Existing-10-20.png", bbox_inches='tight')

new_vs_existing_d[n].iloc[0:,20:30].plot(marker='*', figsize=(20,6), secondary_y=True, legend=False).set_title('New (sum of last %d days) vs existing Death 21-30 as of %s' % (days_to_sum, now_string), fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/New-Death-vs-Existing-20-30.png", bbox_inches='tight')

n = delta.reindex(delta.iloc[[0, -1]].max().sort_values(ascending=False).index, axis = 1).columns.values
delta[n].iloc[-100:,0:5].plot(marker='*', figsize=(20,6), secondary_y=True, legend=False).set_title('New infections per day as of %s' % now_string, fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Delta.png", bbox_inches='tight')

n = delta_d.reindex(delta_d.iloc[[0, -1]].max().sort_values(ascending=False).index, axis = 1).columns.values
delta_d[n].iloc[-100:,0:5].plot(marker='*', figsize=(20,6), secondary_y=True, legend=False).set_title('New death per day as of %s' % now_string, fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Delta-Death.png", bbox_inches='tight')

p = ts.reindex(ts.max().sort_values(ascending=False).index, axis = 1)
p.iloc[50:,0:10].plot  (marker='*',figsize=(20,6), secondary_y=True, legend=False).set_title('Total Confirmed - 1-10 as of %s' % now_string,fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Confirmed-0-10.png", bbox_inches='tight')

p.iloc[50:,10:20].plot(marker='*',figsize=(20,6), secondary_y=True, legend=False).set_title('Total Confirmed - 11-20 as of %s' % now_string,fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Confirmed-10-20.png", bbox_inches='tight')

p.iloc[50:,20:30].plot(marker='*',figsize=(20,6), secondary_y=True, legend=False).set_title('Total Confirmed - Major areas 21-30 as of %s' % now_string,fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Confirmed-20-30.png", bbox_inches='tight')

p_d = ts_d.reindex(ts_d.max().sort_values(ascending=False).index, axis = 1)
p_d.iloc[50:,0:10].plot  (marker='*',figsize=(20,6), secondary_y=True, legend=False).set_title('Total Death - 1-10 as of %s' % now_string,fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Death-0-10.png", bbox_inches='tight')

p_d.iloc[50:,10:20].plot(marker='*',figsize=(20,6), secondary_y=True, legend=False).set_title('Total Death - 11-20 as of %s' % now_string,fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Death-10-20.png", bbox_inches='tight')

p_d.iloc[50:,20:30].plot(marker='*',figsize=(20,6), secondary_y=True, legend=False).set_title('Total Death - 21-30 as of %s' % now_string,fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Death-20-30.png", bbox_inches='tight')

p = ts.reindex(ts.max().sort_values(ascending=False).index, axis = 1)
p.iloc[60:,0:10].plot(marker='*',figsize=(20,6), secondary_y=True, legend=False).set_title('Total Confirmed - 1-10 as of %s' % now_string,fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Confirmed-0-10.png", bbox_inches='tight')

p.iloc[40:,10:20].plot(marker='*',figsize=(20,6), secondary_y=True, legend=False).set_title('Total Confirmed - 11-20 as of %s' % now_string,fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Confirmed-10-20.png", bbox_inches='tight')

p.iloc[70:,20:30].plot(marker='*',figsize=(20,6), secondary_y=True, legend=False).set_title('Total Confirmed - 21-30 as of %s' % now_string,fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Confirmed-20-30.png", bbox_inches='tight')

p_r = ts_rec.reindex(ts_rec.max().sort_values(ascending=False).index, axis = 1)
p_r.iloc[90:,0:10].plot  (marker='*',figsize=(20,6), secondary_y=True, legend=False).set_title('Total Recovered - 1-10 as of %s' % now_string,fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Recovered-0-10.png", bbox_inches='tight')

p_r.iloc[60:,10:20].plot(marker='*',figsize=(20,6), secondary_y=True, legend=False).set_title('Total Recovered - 11-20 as of %s' % now_string,fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Recovered-10-20.png", bbox_inches='tight')

p_r.iloc[100:,20:30].plot(marker='*',figsize=(20,6), secondary_y=True, legend=False).set_title('Total Recovered - 21-30 as of %s' % now_string,fontdict={'fontsize': 22})
plt.legend(loc="best")
plt.savefig("/host/Recovered-20-30.png", bbox_inches='tight')

j = requests.get("https://opendata.arcgis.com/datasets/917fc37a709542548cc3be077a786c17_0.geojson").json()

data = {}
avg  = 0
counter = 0
for i in j['features']:
    if i['properties']['GEN'].startswith("Berlin"):
        gen                  =       i['properties']['GEN']
        death_rate           = float(i['properties']['death_rate'])
        cases                = float(i['properties']['cases'])
        deaths               = float(i['properties']['deaths'])
        cases_per_100k       = float(i['properties']['cases_per_100k'])
        cases_per_population = float(i['properties']['cases_per_population'])
        cases7_per_100k      = float(i['properties']['cases7_per_100k'])
        recovered            = 0    #i['properties']['recovered']
        data[gen] = [death_rate, cases, deaths, cases_per_100k, cases_per_population, cases7_per_100k, recovered]
        avg += cases7_per_100k
        counter += 1
df = pd.DataFrame(data, index = ['Death Rate','Cases','Deaths','Cases per 100k','Cases per Population','Cases per 100k in last 7 days','Recovered'])

berlin    = df.T.sort_values(by='Cases per 100k in last 7 days', ascending=False).drop(labels=['Cases per Population', 'Death Rate', 'Cases per 100k', 'Recovered', 'Cases', 'Deaths'], axis=1).to_html()
berlinavr = avg / counter
iconpath  = "https://icons.23-5.eu"
year      = datetime.today().year
month     = datetime.today().month
day       = datetime.today().day

month_name = {}
month_name[ 1] = "Ja"
month_name[ 2] = "Feb"
month_name[ 3] = "Mar"
month_name[ 4] = "Apr"
month_name[ 5] = "May"
month_name[ 6] = "Jun"
month_name[ 7] = "Jul"
month_name[ 8] = "Aug"
month_name[ 9] = "Sep"
month_name[10] = "Oct"
month_name[11] = "Nov"
month_name[12] = "Dec"

vars = {}
vars['title_date'] = "%s - %s" % (year, month)
vars['year']       = year
vars['month']      = month
vars['month_name'] = month_name[month]
vars['day']        = day
vars['iconpath']   = iconpath

templateLoader = jinja2.FileSystemLoader(searchpath="templates")
templateEnv    = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE  = "month.html"
template       = templateEnv.get_template(TEMPLATE_FILE)
outputText     = template.render(vars=vars, berlin=berlin, berlinavr=berlinavr)

with open("/host/index.html", 'w') as the_file:
    the_file.write(outputText)
